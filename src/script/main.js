require('./utils.js');

class VueComponents {
  appHead() {
    return { 'app-head': { template: require('./components/app-head.html') } };
  }

  appCli() {
    return { 'app-cli': { template: require('./components/app-cli.html') } };
  }

  appNav() {
    return { 'app-nav': { template: require('./components/app-nav.html') } };
  }

  appSidebarLeft() {
    return { 'app-sidebar-left': { template: require('./components/app-sidebar-left.html') } };
  }

  appSidebarRight() {
    return { 'app-sidebar-right': { template: require('./components/app-sidebar-right.html') } };
  }

  appContent() {
    return { 'app-content': { template: require('./components/app-content.html') } };
  }

  appView() {
    return { 'app-view': { template: require('./components/app-view.html') } };
  }
}

function getVueComponents() {
  var vueComponents = new VueComponents();
  var components = {};
  var methods = getAllMethods(vueComponents);
  for (var i = 0; i < methods.length; i++) {
    components = Object.assign(components, vueComponents[methods[i]]());
  }
  return components;
}

function initVue() {
  var vm = new Vue({
    el: '#app',
    components: getVueComponents()
  });
}